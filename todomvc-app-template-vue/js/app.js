(function (window) {

	//定义数组
	const inputList = [
		{
			id: 1,
			name: 'V1.0 原生实现',
			checkstate: false
		},
		{
			id: 2,
			name: 'V2.0 Vue实现',
			checkstate: false
		},
		{
			id: 3,
			name: 'V3.0  React实现',
			checkstate: false
		}
	]

	// Your starting point. Enjoy the ride!
	var vm = new Vue({
		el: '#Root-vue',
		template: '#temp',
		data: {
			todosList: inputList,
			editstate: null,
			filterStat: 'all',
			clearState: false
		},
		created: function () {
			let bool = (this.todosList.filter(function (item) {
				return item.checkstate
			})).length > 0;
			this.clearState = bool;
		},
		computed: {

			todosListFilter: function () {

				switch (this.filterStat) {
					case 'active':
						return this.todosList.filter(function (item) {
							return item.checkstate === false
						})
						break;
					case 'completed':
						return this.todosList.filter(function (item) {
							return item.checkstate === true
						})
						break;
					default:
						return this.todosList
						break;
				}
			},
			itemleft: function () {
				let todoCheckstate = this.todosList.filter(function (item) {
					return item.checkstate === true
				})
				return todoCheckstate.length;
			},
			toggleAllStat: function () {
				return this.todosList.filter(item => item.checkstate === false);
			},
		},
		methods: {


			toggleall: function (event) {
				const check = event.target.checked;
				this.todosList.forEach(function (item) {
					item.checkstate = check;
				})
			},
			
			inputBlur:function(event){
				if((event.target.value).replace(/ /g,'') === ""){
					event.target.style.border="1px solid red"
					return event.target.focus();
				}
				event.target.style.border="1px solid #999"
				this.editstate = null
			},
			
			dblclickItem:function(event,param){
				var focusCount = document.querySelectorAll(".editing").length;
				if(focusCount > 0){
					return;
				}
				this.editstate = param
			},

			addTodo: function (event) {
				const valueStr = event.target.value;
				if(valueStr.replace(/ /g,'')==="") return alert("不能添加空内容！");
				var lastFruits = this.todosList[this.todosList.length - 1];
				var idStr = lastFruits ? lastFruits.id + 1 : 1;
				this.todosList.push({
					id: idStr,
					name: valueStr,
					checkstate: false
				})

				event.target.value = '';
			},

			removeItem: function (index) {
				this.todosList.splice(index, 1);
			},

			removeALLDone: function () {
				this.todosList = this.todosList.filter(function (item) {
					return !item.checkstate
				})
			},

			editInput: function () {
				console.log('editInput');
			},

			editSave: function (item, event) {
				this.editstate = null;
			}
		},
		directives: {
			focuz: {
				inserted: function (el) {
					//console.log("测试");
					el.focus();
				}
			},
			dblfocus: {
				update: function (el, binding) {
					//console.log(binding.value);
					let bool = (vm.todosList.filter(function (item) {
						return item.checkstate
					})).length > 0;
					vm.clearState = bool;
					if (binding.value) {
						el.focus();
					}

				}
			}
		}
	})

	window.onhashchange = function () {
		const hashStr = location.hash.replace("#/", "");
		vm.filterStat = hashStr;
	}

})(window);
